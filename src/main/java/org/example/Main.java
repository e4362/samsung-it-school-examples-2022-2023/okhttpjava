package org.example;

import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

public class Main {


    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static void main(String[] args) {

        System.out.println("Started");

        String IAM_TOKEN = "Bearer t1.9euelZrNxomam53LzpTPiZOPlo-Yx-3rnpWajJiPzJWUnouam87JyI-Ym5Hl8_cKQEBe-e8_MjlA_d3z90puPV757z8yOUD9.l_NYpkwi2eNORValillIMi4J2d8sqzCWLsHcj_67MVkEFoTksNc-IIsrqo7ElTWKopQv93Svz2rM9rAognBECQ";
        String FOLDER_ID = "b1g9snh0l7i56pamb3iq";
        String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";


        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = RequestBody.create(JSON,
                "{'targetLanguageCode': 'en', 'texts': ['Какой же замечательный день!'], 'folderId': '" + FOLDER_ID + "'}");

        Request request = new Request.Builder()
                .url(URL)
                .post(requestBody)
                .header("Authorization", IAM_TOKEN)
                .build();

        try{
            Response response = client.newCall(request).execute();
            System.out.println(response.code());
            String responseStr = response.body().string();
            System.out.println(responseStr);
            JSONObject obj = new JSONObject(responseStr);
            JSONArray arr = obj.getJSONArray("translations");
            for (int i = 0; i < arr.length(); i++){
                System.out.println(arr.getJSONObject(i).getString("text"));
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return;
    }
}